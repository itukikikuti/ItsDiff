import * as vscode from "vscode";
import * as fs from "fs";
import * as path from "path";

let leftUri: string = "";

export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(selectForCompare(context));
	context.subscriptions.push(compareWithSelected(context));
}

export function deactivate() {
}

function selectForCompare(context: vscode.ExtensionContext) {
	return vscode.commands.registerCommand("itsdiff.selectForCompare", (uri: string) => {
		leftUri = decodeURIComponent(uri.toString());
		vscode.window.showInformationMessage(`Selected ${leftUri}.`);
	});
}

function compareWithSelected(context: vscode.ExtensionContext) {
	return vscode.commands.registerCommand("itsdiff.compareWithSelected", (uri: string) => {
		if (leftUri === "") {
			vscode.window.showErrorMessage("Was not selected.");
			return;
		}

		let rightUri: string = decodeURIComponent(uri.toString());
		const panel = vscode.window.createWebviewPanel("itsdiff", `${path.basename(leftUri)} | ${path.basename(rightUri)}`, vscode.ViewColumn.Active, { enableScripts: true });
		
		panel.webview.html = `
			<script>
				const vscode = acquireVsCodeApi();
				function onClick() {
					vscode.postMessage({ command: 'test' });
				}
			</script>
			<table>
				<tr onclick="onClick()"><td>Source.cpp</td><td>Source.cpp</td></tr>
			</table>
		`;
		panel.webview.onDidReceiveMessage(message => {
			vscode.commands.executeCommand("vscode.diff", vscode.Uri.parse(`${leftUri}/Source.cpp`), vscode.Uri.parse(`${rightUri}/Source.cpp`), "Diff");
		})
	});
}
