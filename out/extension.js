"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const path = require("path");
let leftUri = "";
function activate(context) {
    context.subscriptions.push(selectForCompare(context));
    context.subscriptions.push(compareWithSelected(context));
}
exports.activate = activate;
function deactivate() {
}
exports.deactivate = deactivate;
function selectForCompare(context) {
    return vscode.commands.registerCommand("itsdiff.selectForCompare", (uri) => {
        leftUri = decodeURIComponent(uri.toString());
        vscode.window.showInformationMessage(`Selected ${leftUri}.`);
    });
}
function compareWithSelected(context) {
    return vscode.commands.registerCommand("itsdiff.compareWithSelected", (uri) => {
        if (leftUri === "") {
            vscode.window.showErrorMessage("Was not selected.");
            return;
        }
        let rightUri = decodeURIComponent(uri.toString());
        const panel = vscode.window.createWebviewPanel("itsdiff", `${path.basename(leftUri)} | ${path.basename(rightUri)}`, vscode.ViewColumn.Active, { enableScripts: true });
        panel.webview.html = `
			<script>
				const vscode = acquireVsCodeApi();
				function onClick() {
					vscode.postMessage({ command: 'test' });
				}
			</script>
			<table>
				<tr onclick="onClick()"><td>Source.cpp</td><td>Source.cpp</td></tr>
			</table>
		`;
        panel.webview.onDidReceiveMessage(message => {
            vscode.commands.executeCommand("vscode.diff", vscode.Uri.parse(`${leftUri}/Source.cpp`), vscode.Uri.parse(`${rightUri}/Source.cpp`), "Diff");
        });
    });
}
//# sourceMappingURL=extension.js.map